package servlet;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.UserPointsModel;

import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "controllerServlet", value = "/main")
public class ControllerServlet extends HttpServlet {

    protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException {
        response.setStatus(405);
    }
    protected void doPost(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException {
        final HttpSession currentSession = request.getSession();
        if (request.getParameter("clear") != null) {
            currentSession.invalidate();
            System.out.println("clear");
        } else if (request.getParameter("x") != null && request.getParameter("y") != null && request.getParameter("r") != null) {
            getServletContext().getNamedDispatcher("AreaCheckServlet").forward(request, response);
        } else {
            UserPointsModel userPoints = (UserPointsModel) currentSession.getAttribute("points");
            request.setAttribute("points", userPoints);
            getServletContext().getRequestDispatcher("/index.jsp").forward(request, response);
        }
    }
}
