package servlet;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.PointModel;
import model.UserPointsModel;

import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.util.LinkedList;

@WebServlet(name = "AreaCheckServlet", value = "/check_hit")
public class AreaCheckServlet extends HttpServlet {
    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException {
        response.setStatus(405);
    }
    @Override
    protected void doPost(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException {
        final long startExec = System.nanoTime();

        final String contextPath = this.getServletContext().getContextPath();

        final double x;
        final double y;
        final double r;

        try {
            x = Double.parseDouble(request.getParameter("x"));
            y = Double.parseDouble(request.getParameter("y"));
            r = Double.parseDouble(request.getParameter("r"));
        } catch (NumberFormatException | NullPointerException e) {
            response.sendError(400);
            return;
        }

        final boolean result = checkHit(x, y, r);

        final HttpSession currentSession = request.getSession();
        UserPointsModel userPoints = (UserPointsModel) currentSession.getAttribute("points");
        if (userPoints == null) {
            userPoints = new UserPointsModel();
            currentSession.setAttribute("points", userPoints);
        }
        if (userPoints.getPoints() == null)
            userPoints.setPoints(new LinkedList<>());

        final long endExec = System.nanoTime();
        final long executionTime = endExec - startExec;

        final PointModel data = new PointModel();
        data.setX(x);
        data.setY(y);
        data.setR(r);
        data.setHit(result);
        data.setDelay(executionTime);
        data.setTime(System.currentTimeMillis());

        userPoints.getPoints().addFirst(data);

        // Hello
        response.setContentType("text/html;charset=UTF-8");
        final PrintWriter out = response.getWriter();
        out.println("<!DOCTYPE html>");
        out.println("<html>");
        out.println("<head><title>Result</title></head>");
        out.println("<body>");
        out.println("Result: <div id=\"json\">");
        out.println(data.toJSON());
        out.println("</div>");
        out.println("</body>");
        out.println("</html>");
        out.close();

//        request.setAttribute("result", data.toJSON());
//        request.getRequestDispatcher(request.getRequestURI()).forward(request, response);
    }

    private boolean checkHit(final double x, final double y, final double r) {

        boolean first = false;
        boolean second = false;
        boolean third = false;

        if (x >= 0 && y >= 0) {
            first = x*x+y*y<=r*r;
        }
        if (x < 0 && y >= 0) {
            second = ((y <= r/2) && (x > -r));
        }
        if (x < 0 && y < 0) {
            third = Math.abs(x) + Math.abs(y) <= (r / 2);
        }
        return first || second || third;
    }
}