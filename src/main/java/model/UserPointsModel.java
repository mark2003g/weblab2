package model;

import java.beans.JavaBean;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.Objects;

@JavaBean
public class UserPointsModel implements Serializable {
    private LinkedList<PointModel> points;

    public UserPointsModel() {
        super();
    }

    public LinkedList<PointModel> getPoints() {
        return points;
    }

    public void setPoints(LinkedList<PointModel> points) {
        this.points = points;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserPointsModel)) return false;
        UserPointsModel that = (UserPointsModel) o;
        return Objects.equals(that.getPoints(), this.getPoints());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPoints());
    }

    @Override
    public String toString() {
        return "UserPointsModel{PointsList=" + points + "}";
    }

    public String toJSON() {
        StringBuilder stringBuilder = new StringBuilder();
//        stringBuilder.append("[");
        for (int i = 0; i < points.size(); i++) {
            stringBuilder.append(points.get(i).toJSON());
            if (i < points.size() - 1) {
                stringBuilder.append(",");
            }
        }
//        stringBuilder.append("]");
        return stringBuilder.toString();
    }


}