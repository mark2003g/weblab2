package model;

import java.beans.JavaBean;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

@JavaBean
public class PointModel implements Serializable {
    private double x;
    private double y;
    private double r;
    private boolean hit;
    private long delay;
    private long time;

    public PointModel() {
        super();
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getR() {
        return r;
    }

    public boolean isHit() {
        return hit;
    }

    public long getDelay() {
        return delay;
    }

    public long getTime() {
        return time;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    public void setR(double r) {
        this.r = r;
    }

    public void setHit(boolean hit) {
        this.hit = hit;
    }

    public void setDelay(long delay) {
        this.delay = delay;
    }

    public void setTime(long time) {
        this.time = time;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PointModel)) return false;
        PointModel data = (PointModel) o;
        return Double.compare(getX(), data.getX()) == 0 && Double.compare(getY(), data.getY()) == 0 && Double.compare(getR(), data.getR()) == 0 && isHit() == data.isHit() && getDelay() == data.getDelay() && getTime() == data.getTime();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getX(), getY(), getR(), isHit(), getDelay(), getTime());
    }

    @Override
    public String toString() {
        return "PointModel{" +
                "x=" + x +
                ", y=" + y +
                ", r=" + r +
                ", time=" + time +
                ", delay=" + delay +
                ", isHit=" + hit +
                '}';
    }

    public String toJSON() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("{");
        stringBuilder.append("\"x\":").append(x).append(",");
        stringBuilder.append("\"y\":").append(y).append(",");
        stringBuilder.append("\"r\":").append(r).append(",");
        stringBuilder.append("\"time\":").append(time).append(",");
        stringBuilder.append("\"delay\":").append(delay).append(",");
        stringBuilder.append("\"isHit\":").append(hit);
        stringBuilder.append("}");
        return stringBuilder.toString();
    }

}