function formatDate(ms) {
    const date = new Date(ms);
    const day = String(date.getDate()).padStart(2, '0');
    const month = String(date.getMonth() + 1).padStart(2, '0');
    const year = date.getFullYear();
    const hours = String(date.getHours()).padStart(2, '0');
    const minutes = String(date.getMinutes()).padStart(2, '0');
    const seconds = String(date.getSeconds()).padStart(2, '0');

    return `${day}.${month}.${year} ${hours}:${minutes}:${seconds}`;
}

function dictToGET(dict) {
    const res = Object.keys(dict)
        .map(key => `${encodeURIComponent(key)}=${encodeURIComponent(dict[key])}`)
        .join('&');
    return res;
}

class GraphDrawer {
    constructor(canvas) {
        this.canvas = canvas;
        this.ctx = canvas.getContext('2d');
        this.centerX = canvas.width / 2;
        this.centerY = canvas.height / 2;
        this.step = canvas.width / 8;
        this.points = [];
    }

    drawPoint(x, y, color) {
        x *= this.step * 2;
        x += this.centerX;
        y *= this.step * 2;
        y += this.centerY;
        y = this.canvas.height - y;
        this.ctx.beginPath();
        this.ctx.arc(x, y, 2, 0, Math.PI * 2);
        this.ctx.fillStyle = color; // Цвет точки
        this.ctx.fill();
        this.ctx.closePath();
        // this.points.push({'x': x, 'y': y, 'color': color});
    }

    // updateR(r) {
    //     this.draw();
    //     var x;
    //     var y;
    //     for (let i = 0; i < this.points.length; i++) {
    //         x = this.points[i].x;
    //         y = this.points[i].y;
    //         x *= this.step * 2;
    //         x += this.centerX;
    //         y *= this.step * 2;
    //         y += this.centerY;
    //         y = this.canvas.height - y;
    //         this.ctx.beginPath();
    //         this.ctx.arc(x, y, 2, 0, Math.PI * 2);
    //         this.ctx.fillStyle = this.points[i].color; // Цвет точки
    //         this.ctx.fill();
    //         this.ctx.closePath();
    //     }
    // }

    drawGraph() {
        // Система координат
        this.ctx.fillStyle = 'black';
        this.ctx.beginPath();
        this.ctx.moveTo(this.step, this.centerY);
        this.ctx.lineTo(this.canvas.width - this.step, this.centerY);
        this.ctx.moveTo(this.centerX, this.step);
        this.ctx.lineTo(this.centerX, this.canvas.height - this.step);
        this.ctx.stroke();

        // Стрелки
        this.ctx.beginPath();
        this.ctx.moveTo(this.step * 7, this.centerY);
        this.ctx.lineTo(this.step * 7 - 4, this.centerY - 4);
        this.ctx.moveTo(this.step * 7, this.centerY);
        this.ctx.lineTo(this.step * 7 - 4, this.centerY + 4);
        this.ctx.moveTo(this.centerX, this.step);
        this.ctx.lineTo(this.centerX + 4, this.step + 4);
        this.ctx.moveTo(this.centerX, this.step);
        this.ctx.lineTo(this.centerX - 4, this.step + 4);
        this.ctx.stroke();

        // Метки
        this.ctx.beginPath();
        for (let i = 0; i < 5; i++) {
            if (i === 2)
                continue;
            this.ctx.moveTo(i * this.step + this.step * 2, this.centerY - 3);
            this.ctx.lineTo(i * this.step + this.step * 2, this.centerY + 3);
            this.ctx.moveTo(this.centerX - 3, i * this.step + this.step * 2);
            this.ctx.lineTo(this.centerX + 3, i * this.step + this.step * 2);
        }
        this.ctx.stroke();

        // Метки R и R/2
        this.ctx.textAlign = 'center';
        this.ctx.fillText('-R', this.centerX - this.step * 2, this.centerY - 5);
        this.ctx.fillText('-R/2', this.centerX - this.step, this.centerY - 5);
        this.ctx.fillText('R/2', this.centerX + this.step, this.centerY - 5);
        this.ctx.fillText('R', this.centerX + this.step * 2, this.centerY - 5);
        this.ctx.fillText('X', this.centerX + this.step * 3 + 3, this.centerY - 5);
        this.ctx.textAlign = 'left';
        this.ctx.fillText('Y', this.centerX + 5, this.centerY - this.step * 3 + 2);
        this.ctx.fillText('-R', this.centerX + 5, this.centerY + this.step * 2 + 2);
        this.ctx.fillText('-R/2', this.centerX + 5, this.centerY + this.step + 2);
        this.ctx.fillText('R/2', this.centerX + 5, this.centerY - this.step + 2);
        this.ctx.fillText('R', this.centerX + 5, this.centerY - this.step * 2 + 2);
    }

    drawFigures() {
        this.ctx.fillStyle = '#aaf';
        // // Четверть круга в первой четверти
        this.ctx.beginPath();
        this.ctx.arc(this.centerX, this.centerY, this.step, 0, -Math.PI / 2, true);
        this.ctx.lineTo(this.centerX, this.centerY);
        this.ctx.fill();
        //
        // Прямоугольник во второй четверти
        this.ctx.fillRect(this.centerX, this.centerY, -this.step*2, -this.step);
        //
        // // Треугольник в третьей четверти
        this.ctx.beginPath();
        this.ctx.moveTo(this.centerX, this.centerY);
        this.ctx.lineTo(this.centerX - this.step, this.centerY);
        this.ctx.lineTo(this.centerX, this.centerY + this.step);
        this.ctx.closePath();
        this.ctx.fill();
    }

    clear() {
        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
    }

    draw() {
        this.clear();
        this.drawFigures();
        this.drawGraph();
    }

    convertX(x) {
        var res = ((x - this.canvas.getBoundingClientRect().left - this.centerX) / (this.step*2));
        return res;
    }

    convertY(y) {
        var res = ((this.canvas.height - (y - this.canvas.getBoundingClientRect().top) - this.centerY) / (this.step*2));
        return res;
    }
}

class ConnectionManager {
    createRequest()
    {
        var request = false;
        if (window.XMLHttpRequest) {
            //Gecko-совместимые браузеры, Safari, Konqueror
            request = new XMLHttpRequest();
        }
        else if (window.ActiveXObject) {
            //Internet explorer
            try {
                request = new ActiveXObject("Microsoft.XMLHTTP");
            }
            catch (CatchException) {
                request = new ActiveXObject("Msxml2.XMLHTTP");
            }
        }
        if (!request)
        {
            alert("Невозможно создать XMLHttpRequest");
        }
        return request;
    }

    send(handler, data) {
        var request = this.createRequest();
        if (request) {
            request.open("POST", "main?" + dictToGET(data));
            // request.onload = handler;
            request.onload = function () {
                var startIndex = request.response.indexOf('{');
                var endIndex = request.response.lastIndexOf('}') + 1;
                var jsonSubstring = request.response.substring(startIndex, endIndex);
                handler(jsonSubstring);
            }
            request.setRequestHeader('Content-type', 'application/json; charset=UTF-8');
            request.send({"Content-Type": "application/json;charset=utf-8"});
        }
    }

    sendXYR(handler, x, y, r) {
        this.send(handler, {"x": x, "y": y, "r": r});
    }
}

class Table {
    constructor(table) {
        this.table = table;
        this.data = []
    }

    addRow(...args) {
        var row = this.table.insertRow();
        for (let i = 0; i < args.length; i++) {
            if (typeof args[i] === 'number' && !isNaN(args[i])) {
                row.insertCell(i).innerHTML = args[i].toFixed(4);
            } else {
                row.insertCell(i).innerHTML = args[i];
            }
        }

        this.data.push({
            'x': args[0],
            'y': args[1],
            'r': args[2],
            'time': args[3],
            'delay': args[4],
            'isHit': args[5]
        })
    }

    clear() {
        let size = this.table.rows.length - 1;
        for (var i = 0; i < size; i++) {
            this.table.deleteRow(1);
        }
        this.data = [];
    }

    getData() {
        return this.data;
    }
}

class Panel {
    constructor(graphDrawer, table) {
        this.button_x = null;
        this.input_y = null;
        this.button_r = null;
        this.oldY = 0;
        this.graphDrawer = graphDrawer;
        this.table = table;
    }

    setValueX(button) {
        if (this.button_x != null) {
            this.button_x.classList.remove('red_bg');
        }
        this.button_x = button;
        this.button_x.classList.add('red_bg');
    }

    setValueY(input) {
        if (input.value.length > 6) {
            input.value = this.oldY;
            return;
        }
        input.value = input.value.replace(',', '.');
        this.oldY = input.value;
        if (isNaN(input.value) || input.value[input.value.length-1] === '.') {
            input.classList.add('input-error');
        } else if (input.value === '') {
            input.classList.add('input-error');
        } else if (input.value <= -5 || input.value >= 5) {
            input.classList.add('input-error');
        } else {
            input.classList.remove('input-error');
            this.input_y = input;
            this.input_y.value = parseFloat(input.value);
            // this.oldY = this.input_y.value;
        }
    }

    setTable(table) {
        this.table = table;
    }

    setValueR(button) {
        if (this.button_r != null) {
            this.button_r.classList.remove('blue_bg');
        }
        this.button_r = button;
        this.button_r.classList.add('blue_bg');

        updateR(this.graphDrawer, this.table.getData(), this.getR());
    }

    getX() {
        if (this.button_x == null) {
            return null
        }
        return parseFloat(this.button_x.value);
    }

    getY() {
        if (this.input_y == null) {
            return null
        }
        if (this.input_y.value.length > 6) {
            return null;
        }
        if (isNaN(this.input_y.value) || this.input_y.value[this.input_y.value.length-1] === '.') {
            return null;
        } else if (this.input_y.value === '') {
            return null;
        } else if (this.input_y.value <= -3 || this.input_y.value >= 5) {
            return null;
        }
        return parseFloat(this.input_y.value);
    }

    getR() {
        if (this.button_r == null) {
            return null
        }
        return parseFloat(this.button_r.textContent);
    }
}

function submit(panel, connectionManager, graphDrawer, table) {
    var r = panel.getR();
    var x = panel.getX();
    var y = panel.getY();
    if (x == null) {
        alert("X is incorrect value. Please select a value.");
        return;
    }
    if (y == null) {
        alert("Y is incorrect value. Please enter a value.");
        return;
    }
    if (r == null) {
        alert("R is incorrect value. Please select a value.");
        return;
    }
    connectionManager.sendXYR(function (htmlText) {
        var startIndex = htmlText.indexOf('{');
        var endIndex = htmlText.lastIndexOf('}') + 1;
        var jsonSubstring = htmlText.substring(startIndex, endIndex);
        console.log(jsonSubstring);
        const jsObject = JSON.parse(jsonSubstring);
        table.addRow(x, y, r, formatDate(jsObject.time), jsObject.delay, jsObject.isHit);
        graphDrawer.drawPoint(x/r, y/r, jsObject.isHit ? "#00FF00" : "#FF0000");
        // if (jsObject.status) {
        //     table.addRow(jsObject.x, jsObject.y, jsObject.r, formatDate(jsObject.time * 1000), jsObject.delay, jsObject.isHit);
        // } else {
        //     alert(jsObject.msg);
        // }
    }, x, y, r)
}

function clear(panel, connectionManager, graphDrawer, table) {
    alert("clear");
    graphDrawer.draw();
    table.clear();
    connectionManager.send({'clear': true});
}

function updateR(graphDrawer, points, r) {
    graphDrawer.draw();
    for (let i = points.length-1; i >= 0 ; i--) {
        graphDrawer.drawPoint(points[i].x / r, points[i].y / r, points[i].isHit ? "#00FF00" : "#FF0000");
    }
}