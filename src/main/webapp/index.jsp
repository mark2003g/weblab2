<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta http-equiv="Content-Type" content="text/html" charset="UTF-8" />
    <link rel="stylesheet" href="static/styles/base.css">
    <script src ="static/scripts/base.js"></script>
</head>
<body>
<table class="container">
    <tr><header>Mark Gelya, P3232<br>Variant: 2119932</header></tr>
    <tr>
        <td class="col2">
            <canvas id="graph" height="300" width="300"></canvas>
        </td>
    </tr>
    <script>
        var graphDrawer = new GraphDrawer(document.getElementById('graph'));
        graphDrawer.draw();
        var connectionManager = new ConnectionManager();
        var table;
        var panel = new Panel(graphDrawer, table);
        var tableData = {"points": [${points.toJSON()}]};
    </script>

    <tr>
        <td class="col2">
            <table class="container">
                <td class="col2">
                    <table id="panel">
                        <tr>
                            <td class="panel-label">
                                Value X:
                            </td>
                            <td class="panel-input">
<%--                                <button class="btnX" onclick="panel.setValueX(this)">-5</button>--%>
<%--                                <button class="btnX" onclick="panel.setValueX(this)">-4</button>--%>
<%--                                <button class="btnX" onclick="panel.setValueX(this)">-3</button>--%>
<%--                                <button class="btnX" onclick="panel.setValueX(this)">-2</button>--%>
<%--                                <button class="btnX" onclick="panel.setValueX(this)">-1</button>--%>
<%--                                <button class="btnX" onclick="panel.setValueX(this)">0</button>--%>
<%--                                <button class="btnX" onclick="panel.setValueX(this)">1</button>--%>
<%--                                <button class="btnX" onclick="panel.setValueX(this)">2</button>--%>
<%--                                <button class="btnX" onclick="panel.setValueX(this)">3</button>--%>
                                    <% int[] values = {-4, -3, -2, -1, 0, 1, 2, 3, 4}; %>
                                    <% for (int value : values) { %>
                                    <label>
                                        <input type="radio" name="value-x" class="btnX" onclick="panel.setValueX(this)" value="<%= value %>" />
                                        <%= value %>
                                    </label>
                                    <% } %>

                            </td>
                        </tr>
                        <tr>
                            <td class="panel-label">
                                Value Y:
                            </td>
                            <td class="panel-input">
                                <label>
                                    <input id="inputY" class="inputY" type="text" value="0" oninput="panel.setValueY(this)">
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td class="panel-label">
                                Value R:
                            </td>
                            <td class="panel-input">
                                <button class="btnR" onclick="panel.setValueR(this)">1</button>
                                <button class="btnR" onclick="panel.setValueR(this)">2</button>
                                <button class="btnR" onclick="panel.setValueR(this)">3</button>
                                <button class="btnR" onclick="panel.setValueR(this)">4</button>
                                <button class="btnR" onclick="panel.setValueR(this)">5</button>
                            </td>
                        </tr>
                        <tr><td></td><td class="buttons"><input type="submit" value="Send" onclick="submit(panel, connectionManager, graphDrawer, table)"><input type="button" value="Clear" onclick="graphDrawer.draw(); table.clear(); connectionManager.send(function(){}, {'clear': true})"/></td></tr>
                    </table>
                </td>
            </table>
        </td>
    </tr>
    <tr>
        <td class="col2">
            <table id="result_table">
                <tr><th class="red_bg">X</th><th class="green_bg">Y</th><th class="blue_bg">R</th><th class="pink_bg">Time</th><th class="skyblue_bg">Execute time (ns)</th><th class="yellow_bg">Is hit</th></tr>
            </table>
            <script>
                panel.setValueY(document.getElementById("inputY"));
                table = new Table(document.getElementById("result_table"));
                panel.setTable(table);

                for (let i = tableData.points.length-1; i >= 0 ; i--) {
                    table.addRow(tableData.points[i].x, tableData.points[i].y, tableData.points[i].r, formatDate(tableData.points[i].time), tableData.points[i].delay, tableData.points[i].isHit);
                    graphDrawer.drawPoint(tableData.points[i].x, tableData.points[i].y, tableData.points[i].isHit ? "#00FF00" : "#FF0000");
                }

                graphDrawer.canvas.addEventListener('click', function (event) {
                    var r = panel.getR();
                    var x = graphDrawer.convertX(event.clientX);
                    var y = graphDrawer.convertY(event.clientY);
                    if (r != null) {
                        connectionManager.sendXYR(function (result) {
                            const jsObject = JSON.parse(result);
                            graphDrawer.drawPoint(x, y, jsObject.isHit ? "#00FF00" : "#FF0000");
                            table.addRow(jsObject.x, jsObject.y, jsObject.r, formatDate(jsObject.time), jsObject.delay, jsObject.isHit);
                        }, x*r, y*r, r)
                    } else {
                        alert("R cannot be null.")
                    }
                });
            </script>
        </td>
    </tr>
</table>
</body>
</html>
